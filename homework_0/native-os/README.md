# Install Git

[다운로드 링크](https://git-scm.com/downloads)

1. 위의 링크에서 본인의 OS 에 맞는 환경에 맞춰 다운로드 후 인스톨
2. 기본 설정으로 설치할 것을 권장
3. 터미널에서 다음 명령어를 실행하여 버전 확인

```bash
git --version
```

# Install Python 3

[참고 링크](https://realpython.com/installing-python/)

## Windows

[다운로드 링크](https://www.python.org/downloads/)

1. 상단의 참고 링크를 참조하여 안내대로 설치
2. 인스톨러의 <code>Add Python 3.8 to PATH</code>를 체크하거나 추후 별도로 <code>PATH</code>환경 변수에 추가할 것
3. Powershell 에서 다음 명령어로 버전 확인

```bash
python --version
```

결과가 <code>Python 3.7</code> 버전 이하일 경우 재설치 필요

## macOS

[다운로드 링크](https://www.python.org/downloads/)

### Homebrew 로 설치

1. <Code>Homebrew</Code> 설치 후 다음 명령어 실행

```bash
brew install python3
```

### 공식 인스톨러로 설치

[다운로드 링크](https://www.python.org/downloads/release/python-385/)

1. 공식 홈페이지에서 인스톨러 다운로드 후 설치

### 설치 확인

```bash
python --version
```

또는

```bash
python3 --version
```

⚠ 아래 명령어로 올바른 Python 버전이 출력될 경우 앞으로 실습 중 터미널에서 <code>python</code> 명령어는 모두 <code>python3</code> 로 대체하여 사용하여야 함.