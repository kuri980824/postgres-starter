# Docker Desktop for mac

[공식 문서](https://docs.docker.com/docker-for-mac/install/)

## 필요 조건

- mac 하드웨어 2011 이후 모델
- macOS 10.13 이상

## Git 

1. 자신의 현재 <code>git</code> 버전을 확인 후 2.26 보다 낮을 경우 패키지 업데이트 할 것

```bash
git --version
```

### Git 설치

[공식 문서](https://git-scm.com/book/ko/v2/%EC%8B%9C%EC%9E%91%ED%95%98%EA%B8%B0-Git-%EC%84%A4%EC%B9%98)
[다운로드 링크](https://git-scm.com/download/mac)

1. <code>homebrew</code> 로 설치: <code>brew install git</code> 또는
2. 터미널에 <code>git</code> 실행하여 설치

## Docker

### Docker 다운로드

[다운로드 링크](https://hub.docker.com/editions/community/docker-ce-desktop-mac/)

1. 'Get Stable'로 다운로드

### 설치

[공식 문서](https://docs.docker.com/docker-for-mac/install/#install-and-run-docker-desktop-on-mac)

- 파인더에서 더블 클릭하여 열기
- Application 으로 드래그 하여 설치
- Application 폴더에서 <code>Docker.app</code> 더블클릭하여 실행

### Docker Desktop 설정

1. Docker Desktop 열기 (런치패드, 상단 바 아이콘 등)
1. 설정 (상단의 톱니바퀴 또는 Preference)
1. Resources - Advanced
    - 본인 사양에 맞추어 적절히 설정
1. Resources - File sharing
    - '+' 버튼을 눌러 본인이 원하는 디렉토리를 지정
    - 기본적으로 <code>/Users</code>, <code>/Volume</code>, <code>/private</code>, <code>/tmp</code>, <code>/var/folders</code> 가 지정되어 있음.
  

### Docker version 확인하기 / Git version 확인하기

터미널에서

1. docker 버전 확인
    - <code>docker version</code> 실행
    - <code>docker-compose version</code> 실행
1. <code>git --version</code> 실행

결과 **스크린샷**을 찍고 과제에 포함

🎉🎉 설치 완료! 🎉🎉

## Python 버전 보기

터미널에서 다음을 실행하고 스크린샷을 결과에 포함.

```bash
docker run --rm python python --version
```

결과 예시

![Cap 2020-09-05 20-00-24-842](https://user-images.githubusercontent.com/32762296/92303699-72f38300-efb2-11ea-8d74-6cb25c2fff78.jpg)


## 이 방법대로 설치한 경우...

실습수업 중

1. '터미널'을 열어야 하는 안내에서
    - <code>iTerm</code> 등 적절한 터미널 에뮬레이터를 시작할 것
1. '프로젝트 폴더'를 열어야 하는 안내에서
    1. 본인이 이번 학기 수업 중 코드를 저장할 적절한 위치에 폴더를 새로 만들고 사용
    2. 해당 폴더는 Docker Settings - Resources - File sharing 에 등록된 **폴더 아래에 있어야 함!**
