# Docker Desktop for Windows without WSL 2

[공식 문서](https://docs.docker.com/docker-for-windows/install/)

## 필요 조건

- 가상화 지원 CPU 및 활성화 (BIOS/UEFI)
- Windows 10 64-bit: Pro, Enterprise, or Education (Build 16299 이상).
- 4GB 이상 RAM

## 가상화 기능 활성화

- BIOS 또는 UEFI (보편적으로 부팅 중 F2, Delete 연타, Thinkpad, Sony VAIO 등 독립적인 버튼이 있는 경우가 있으니 제조사 매뉴얼 확인) 진입
- (Intel CPU) Intel Virtualization 또는 그와 유사한 이름의 기능을 활성화(Enabled)
- (AMD CPU) AMD-V 또는 그와 유사한 이름의 기능을 활성화(Enabled)

## Windows 10 Build 16299 이상 업데이트

[공식 문서](https://support.microsoft.com/ko-kr/help/4028685/windows-10-get-the-update)

1. 시작 - 설정(톱니바퀴) - 업데이트 및 보안
1. 화면 우측 또는 하단에 'OS 빌드 정보'
1. 하단의 'Windows 사양' - 'OS 빌드' 확인

버전 예시

![Cap 2020-09-04 20-37-30-313](https://user-images.githubusercontent.com/32762296/92235481-b4baf580-eeee-11ea-9d67-6fec7a1a022d.png)

### 버전이 Build 16299 이하일 경우

1. 시작 - 설정(톱니바퀴) - 업데이트 및 보안
1. 'Windows 10, 버전 2004의 기능 업데이트' 안내
1. '지금 다운로드 및 설치' 클릭
1. 안내에 따라 재부팅하고 버전을 다시 확인

## Hyper-V 활성화

[공식 문서](https://docs.microsoft.com/ko-kr/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v)

관리자로 Powershell 켜기

1. 시작 - <code>powershell</code> 검색
    1. 우클릭하고 <code>관리자 권한으로 실행</code>
    1. 또는 우측 패널에서 <code>관리자로 실행</code>
1. <code>관리자: Windows PowerShell</code> 창에서 다음 명령을 차례로 수행

명령 1
```powershell
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

실행이 완료되면 

3. **재부팅**

## Docker for Windows 설치

[다운로드 링크](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)

1. 위의 링크에서 'Get Stable' 클릭하여 설치 파일 다운로드
1. 설치 파일 실행하여 WSL 실행하지 않고 설치
1. 설치 후 윈도우 로그아웃 / 로그인

설치 과정 중 WSL 관련 옵션 끄기 (체크 끄기, 'Use Hyper-V', 'Not Now' 선택)

![Cap 2020-09-02 19-21-22-660](https://user-images.githubusercontent.com/32762296/92245873-e9837880-eeff-11ea-99a3-e806a6610173.png)

'Enable WSL 2 Windows Features' 체크 **끄기**

![Cap 2020-09-02 19-24-33-332](https://user-images.githubusercontent.com/32762296/92245921-015afc80-ef00-11ea-8740-f1540a4a75d1.png)

'Use Hyper-V' 선택

![Cap 2020-09-02 19-26-11-590](https://user-images.githubusercontent.com/32762296/92245925-0324c000-ef00-11ea-8b58-07d80881fe81.png)

**'Not now'** 선택

1. 설치 프로그램 안내대로 재부팅 (로그아웃/로그인)

## Docker Desktop 설정

1. Docker Desktop 열기 (시작, 바탕화면 바로가기, 우측 하단 트레이 등)
1. 설정 (상단의 톱니바퀴 또는 우측 하단 트레이 우클릭 - Settings)
1. Resources - Advanced
    - 본인 사양에 맞추어 적절히 설정
1. Resources - File sharing
    - '+' 버튼을 눌러 본인이 원하는 디렉토리를 지정
    - 디렉토리는 다수를 지정할 수 있으며 지정한 디렉토리의 하위 경로만 Docker 에서 접근할 수 있다.

예시 화면

![Cap 2020-09-02 19-54-52-114](https://user-images.githubusercontent.com/32762296/92246637-02405e00-ef01-11ea-9394-78fbd7d1eafe.png)

![Cap 2020-09-02 19-55-47-200](https://user-images.githubusercontent.com/32762296/92246641-03718b00-ef01-11ea-8224-32548c2d0c11.png)


## Git for Windows 설치

[다운로드 링크](https://git-scm.com/downloads)

1. Windows 버전 다운로드 후 실행
1. 기본 설정으로 설치

## Docker version 확인하기 / Git version 확인하기

'Powershell' 터미널에서

1. docker 버전 확인
    - <code>docker version</code> 실행
    - <code>docker-compose version</code> 실행
1. <code>git --version</code> 실행

결과 **스크린샷**을 찍고 과제에 포함

결과 예시

Image #1: Docker version / Docker compose version

![Cap 2020-09-02 22-13-55-493](https://user-images.githubusercontent.com/32762296/92246213-67e01a80-ef00-11ea-8f07-4e54adb9f033.png)

Image #2: Git version

![Cap 2020-09-04 17-10-44-024](https://user-images.githubusercontent.com/32762296/92246221-6adb0b00-ef00-11ea-9061-9b2d0d4186fb.png)


🎉🎉 설치 완료! 🎉🎉

## Python 버전 보기

터미널에서 다음을 실행하고 스크린샷을 결과에 포함.

```bash
docker run --rm python python --version
```

결과 예시

![Cap 2020-09-05 20-00-24-842](https://user-images.githubusercontent.com/32762296/92303699-72f38300-efb2-11ea-8d74-6cb25c2fff78.jpg)


# 이 방법대로 설치한 경우...

실습수업 중

1. '터미널'을 열어야 하는 안내에서
    - <code>Powershell</code> 을 시작할 것
1. '프로젝트 폴더'를 열어야 하는 안내에서
    1. 본인이 이번 학기 수업 중 코드를 저장할 적절한 위치에 폴더를 새로 만들고 사용
    2. 해당 폴더는 Docker Settings - Resources - File sharing 에 등록된 **폴더 아래에 있어야 함!**
