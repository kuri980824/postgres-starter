# DB Homework

## Homework 0: Setup

### 목표

아래의 실습환경 중 **한 가지**를 선택하여 실습 환경을 구축하고 완료되었음을 보여주는 **스크린샷** 묶음과 자신이 설치한 실습 환경을 기입한 .txt 파일을 .zip으로 **압축하여 제출**하세요.

1. Docker Desktop for Windows (Pro, Enterprise, Education) / No WSL 2
1. Docker Desktop for Windows (Home+) / WSL 2
1. Docker Desktop for mac
1. Docker Engine / Docker Compose on Linux Distro
1. OS Native

<span style="color:red;">⚠ **주의!** 각 안내서에서 번호가 매겨진 항목은 빠짐없이 순서대로 실행하여야 합니다. ⚠</span>

⚠ 제공되는 실습 조건과 동일함을 보장하기 위해서는 Docker 설치를 강력히 권장합니다. 그러나 PC 사양 등의 문제로 OS 에 직접 필요한 프로그램을 설치할 수 있습니다. 그러나 실습환경 문제로 발생하는 문제는 조교가 곧바로 해결 불가할 수 있으며, **이후 모든 학습 안내서는 Docker 환경을 기본으로 작성됩니다.**

상기한 방법을 올바르게 구축했다면 다음의 서비스/프로그램이 **모두** 잘 설치되어 동작하고 있어야 합니다.

- Python 3.7 이상
- Git 2.26 이상

또는
- Docker 19 이상

터미널에서 각 프로그램의 버전을 출력하는 결과를 스크린샷으로 찍고, .zip으로 **압축** 해서 제출합니다.

### 주의사항

- 결과물은 **3개**(Docker 설치) 또는 **2개**(Native 설치)의 명령어를 실행한 결과의 스크린샷과 **1개**의 텍스트 파일로 이루어진 .zip 압축파일 **1개** 입니다.
- 스크린샷은 가급적 .jpg 또는 .png 로 캡쳐합니다.
- 압축파일은 .zip으로 압축되어야 합니다! 다른 포맷으로 압축된 경우 감점됩니다.

예1: Docker 설치한 경우
```bash
homework_0.zip
├── description.txt
├── docker-version.png
├── git-version.png
└── python-version.png
```

예2: Docker 설치하지 않은 경우
```bash
homework_0.zip
├── description.txt
├── git-version.png
└── python-version.png
```

### 정답 예시
#### 1. Docker / Windows / WSL 2
```text
# description.txt
선택 환경:
2. Windows + Docker WSL 2
```
Image #1: Docker version / Docker compose version

![Cap 2020-09-04 16-55-53-695](https://user-images.githubusercontent.com/32762296/92216025-aeb61c00-eed0-11ea-84ce-ac8ab35b6f87.png)

Image #2: Git version

![Cap 2020-09-04 16-53-27-630](https://user-images.githubusercontent.com/32762296/92216099-cdb4ae00-eed0-11ea-979b-38d08790318e.png)


Image #3: Python version

![Cap 2020-09-04 16-56-26-297](https://user-images.githubusercontent.com/32762296/92216248-03f22d80-eed1-11ea-82fc-c6fcd41a4d6c.png)


<!-- Image #3: Postgres - psql query

![Cap 2020-09-04 17-00-40-755](https://user-images.githubusercontent.com/32762296/92216209-fa68c580-eed0-11ea-9e56-43ce699900c1.png) -->


### 2. Docker / Windows / No WSL

```text
선택환경:
1. Windows Pro + Docker (No WSL 2)
```

Image #1: Docker version

![Cap 2020-09-02 22-13-55-493](https://user-images.githubusercontent.com/32762296/92216511-592e3f00-eed1-11ea-8542-f7d4d877c00e.png)

Image #2: Git version

![Cap 2020-09-04 17-10-44-024](https://user-images.githubusercontent.com/32762296/92216681-9397dc00-eed1-11ea-8db4-1295cb31c3af.png)

Image #3: 예시 1과 동일