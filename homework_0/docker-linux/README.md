# Docker Engine and Compose for Linux Distro

[공식 문서](https://docs.docker.com/engine/install/)

## 필요 조건

- CentOS 7 이상 또는
- Debian 9, 10 이상 또는
- Ubuntu 16.04, 18.04, 20.04 또는
- Fedora 30 이상

## Git 설치

1. 자신의 현재 <code>git</code> 버전을 확인 후 2.26 보다 낮을 경우 패키지 업데이트 할 것. [참고](https://git-scm.com/download/linux)

```bash
git --version
```

## Docker Engine 설치

자신의 배포판에 맞는 설치 안내서로 이동

1. 기존 배포판 기본 버전의 Docker 를 제거
2. 새로운 레포지토리 등록
3. 패키지 관리자 (apt, yum 등) 로 설치

자세한 명령어는 [공식 문서](https://docs.docker.com/engine/install/)의 배포판 별 설치 방법을 참조

## 설치 후 과정

[공식 문서](https://docs.docker.com/engine/install/linux-postinstall/)

docker 는 <code>root</code> 권한으로 동작한다. 따라서 일반 유저가 docker 를 실행할 권한을 받기 위해서는 <code>docker</code> 그룹에 현재 사용자를 포함해야 한다.

다음의 명령어를 차례로 터미널에 실행

1. <code>docker</code> group 생성

```bash
sudo groupadd docker
```
2. 현재 사용자를 <code>docker</code> 그룹에 등록

```bash
sudo usermod -aG docker $USER
```

3. 로그아웃 후 재로그인

## Docker compose 설치

[공식 문서](https://docs.docker.com/compose/install/)

터미널에서 다음을 실행

1. 다운로드 + 설치스크립트 실행

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
2. 실행 파일 권한 부여

```bash
sudo chmod +x /usr/local/bin/docker-compose
```

<code>docker-compose</code> 실행 후 에러가 난다면 심볼링 링크를 추가적으로 생성

1. 심볼릭 링크 생성

```bash
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

## Docker version 확인하기 / Git version 확인하기

터미널에서

1. docker 버전 확인
    - <code>docker version</code> 실행
    - <code>docker-compose version</code> 실행
1. <code>git --version</code> 실행
    - git 버전이 너무 낮을 경우 (2.25 이하). [레포지토리 업데이트 후](https://git-scm.com/download/linux) 패키지 업데이트 필요


결과 **스크린샷**을 찍고 과제에 포함

🎉🎉 설치 완료! 🎉🎉

## Python 버전 보기

터미널에서 다음을 실행하고 스크린샷을 결과에 포함.

```bash
docker run --rm python python --version
```

결과 예시

![Cap 2020-09-05 20-00-24-842](https://user-images.githubusercontent.com/32762296/92303699-72f38300-efb2-11ea-8d74-6cb25c2fff78.jpg)


# 이 방법대로 설치한 경우...


실습수업 중

1. '터미널'을 열어야 하는 안내에서
    - 본인이 사용하는 터미널 에뮬레이터 실행
1. '프로젝트 폴더'를 열어야 하는 안내에서
    1. 본인이 이번 학기 수업 중 코드를 저장할 적절한 위치에 폴더를 새로 만들고 사용  
