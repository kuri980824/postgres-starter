# Docker Desktop for Windows with WSL 2

[공식 문서](https://docs.docker.com/docker-for-windows/install-windows-home/)

## 필요 조건

- 가상화 지원 CPU 및 활성화 (BIOS/UEFI)
- Windows 10 64-bit Home 이상 Build 2004
- 4GB 이상 RAM

## 가상화 기능 활성화

- BIOS 또는 UEFI (보편적으로 부팅 중 F2, Delete 연타, Thinkpad, Sony VAIO 등 독립적인 버튼이 있는 경우가 있으니 제조사 매뉴얼 확인) 진입
- (Intel CPU) Intel Virtualization 또는 그와 유사한 이름의 기능을 활성화(Enabled)
- (AMD CPU) AMD-V 또는 그와 유사한 이름의 기능을 활성화(Enabled)

## Windows 10 Build 2004 업데이트

[공식 문서](https://support.microsoft.com/ko-kr/help/4028685/windows-10-get-the-update)

1. 시작 - 설정(톱니바퀴) - 업데이트 및 보안
1. 화면 우측 또는 하단에 'OS 빌드 정보'
1. 하단의 'Windows 사양' - '버전' 확인

버전 예시

![Cap 2020-09-04 20-37-30-313](https://user-images.githubusercontent.com/32762296/92235481-b4baf580-eeee-11ea-9d67-6fec7a1a022d.png)

### 버전이 2004 이하일 경우

1. 시작 - 설정(톱니바퀴) - 업데이트 및 보안
1. 'Windows 10, 버전 2004의 기능 업데이트' 안내
1. '지금 다운로드 및 설치' 클릭
1. 안내에 따라 재부팅하고 버전을 다시 확인

### 2004 업데이트 불가능한 경우

'지금 다운로드 및 설치' 대신 다음과 같은 화면을 볼 수 있음. [링크](https://www.quasarzone.com/bbs/qf_sw/views/28403)

이 경우는 **설치를 더 진행할 수 없음**. 상위 폴더의 <code>docker-windows-wsl</code> 또는 <code>native</code> 방법으로 설치할 것.

## WSL 2 기능 활성화

[공식 문서](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

관리자로 Powershell 켜기

1. 시작 - <code>powershell</code> 검색
    1. 우클릭하고 <code>관리자 권한으로 실행</code>
    1. 또는 우측 패널에서 <code>관리자로 실행</code>
1. <code>관리자: Windows PowerShell</code> 창에서 다음 명령을 차례로 수행

명령 1
```powershell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
```
명령 2
```powershell
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

두 명령이 실행 완료된 후

3. **재부팅**

완료 예시

![Cap 2020-09-02 16-02-05-709](https://user-images.githubusercontent.com/32762296/92237022-960a2e00-eef1-11ea-8cac-fb22974ed3f0.png)


## WSL 2 Linux 커널 설치

[공식 문서](https://docs.microsoft.com/ko-kr/windows/wsl/wsl2-kernel)

공식 문서의 '최신 WSL2 Linux 커널 업데이트 패키지를 다운로드' 링크의 파일을 다운받고 설치

1. **관리자**로 Powershell을 열고 다음을 실행

```powershell
wsl --set-default-version 2
```

결과가 밑의 예시와 다를 경우 재부팅 후 다시 시도

완료 예시

![Cap 2020-09-02 16-12-30-097](https://user-images.githubusercontent.com/32762296/92237018-95719780-eef1-11ea-9394-8ad1f0202584.png)

## WSL 2 리소스 설정

[공식 문서](https://docs.microsoft.com/ko-kr/windows/wsl/wsl-config#wsl-2-settings)


1. 탐색기 주소창에 <code>%userprofile%</code> 입력하여 사용자 폴더로 이동.
1. 새로운 텍스트 파일 생성하여 메모장 등 에디터로 열기
1. 파일 안에 다음 내용을 참고하여 기입.
    - <code>memory</code>: 자신의 메모리가 4GB 면 512MB, 그 이상일 경우 1/4 정도을 GB 단위의 정수로 기입
    - <code>processors</code>: 자신의 CPU 코어가 2라면 1로 기입 4라면 2로 기입. 그 외에는 해당 행을 지울 것.
1. 파일을 저장한 후 닫고, 이름을 .wslconfig 으로 바꿀 것.
    - .txt 확장자까지 삭제해야 함.
    - 확장자가 보이지 않으면 탐색기 상단의 리본 메뉴 - '보기' 탭 - 우측 '표시/숨기기'의 '파일 확장명' 체크 표시
    - 다음 재부팅 (또는 wsl 서비스 재시작) 이후부터 적용됨. 당장 재시작할 필요는 없음.

<code>.wslconfig</code> 파일 예시

```text
[wsl2]
memory=2GB
processors=1
```

## Microsoft Store 에서 Ubuntu 20.04 설치

[다운로드 링크](https://www.microsoft.com/ko-kr/p/ubuntu-2004-lts/9n6svws3rx71?cid=msft_web_appsforwindows_chart&activetab=pivot:overviewtab)

1. 위의 링크 또는 시작 - 'store' 검색 - 'Microsoft Store' 클릭하여 'Microsoft Store' 열기
1. 'Ubuntu' 검색
1. 'Ubuntu 20.04' 설치
1. 설치 완료 후, 시작 - 'Ubuntu 20.04' 시작 - **Ubuntu Terminal 창 열림**
    - 우클릭 - '시작 화면에 고정' 또는 '자세히' - '작업 표시줄에 고정' 하여 바로가기를 만드는 것을 권장

Ubuntu Terminal 에서

1. 초기 설치 진행 후, username 및 password 작성
    - Windows 사용자명이나 비밀번호와 관계없이 새로 작성
    - 잊어버릴 경우 Ubuntu 20.04 다시 설치
1. 패키지 업데이트

```bash
sudo apt update && sudo apt upgrade -y
```

비밀번호 입력하여 <code>sudo</code> 권한 획득 후 패키지 전체 업데이트


⚠ 안내 - 용량이 부담될 경우 Microsoft Store 검색 창에 'Debian' 을 검색하여 설치. 수업 중 사용할 대부분의 명령어가 호환됨. 그러나 git 은 별도로 설치해야 함. 위의 apt 명령어로 패키지 업데이트 후 [git 공식 문서](https://git-scm.com/download/linux) 참조


## Docker for Windows 설치

[다운로드 링크](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)

1. 위의 링크에서 'Get Stable' 클릭하여 설치 파일 다운로드
1. 설치 파일 실행하여 기본 설정으로 설치
1. 설치 후 윈도우 로그아웃 / 로그인


## Docker for Windows 설정

1. Docker Desktop 열기 (시작, 바탕화면 바로가기, 우측 하단 트레이 등)
1. 설정 (상단의 톱니바퀴 또는 우측 하단 트레이 우클릭 - Settings)
1. Resources - WSL integration
1. <code>Ubuntu 20.04</code> 토글

예시 화면

![Cap 2020-09-02 16-22-11-972](https://user-images.githubusercontent.com/32762296/92243750-e8048100-eefc-11ea-8e7d-2aebfe8141b0.png)


## Docker version 확인하기 / Git version 확인하기

'Ubuntu 20.04' 터미널에서

1. docker 버전 확인
    - <code>docker version</code> 실행
    - <code>docker-compose version</code> 실행
1. <code>git --version</code> 실행

결과 **스크린샷**을 찍고 과제에 포함

결과 예시

Image #1: Docker version / Docker compose version

![Cap 2020-09-04 16-55-53-695](https://user-images.githubusercontent.com/32762296/92216025-aeb61c00-eed0-11ea-84ce-ac8ab35b6f87.png)

Image #2: Git version

![Cap 2020-09-04 16-53-27-630](https://user-images.githubusercontent.com/32762296/92216099-cdb4ae00-eed0-11ea-979b-38d08790318e.png)

🎉🎉 설치 완료! 🎉🎉

## Python 버전 보기

터미널에서 다음을 실행하고 스크린샷을 결과에 포함.

```bash
docker run --rm python python --version
```

결과 예시

![Cap 2020-09-05 20-00-24-842](https://user-images.githubusercontent.com/32762296/92303699-72f38300-efb2-11ea-8d74-6cb25c2fff78.jpg)

# 이 방법대로 설치한 경우...

실습수업 중

1. '터미널'을 열어야 하는 안내에서
    - <code>Ubuntu 20.04</code> 를 시작할 것
1. '프로젝트 폴더'를 열어야 하는 안내에서
    1. 탐색기 주소 칸에 다음을 입력 <code>\\\\wsl$\Ubuntu-20.04\home</code>
    1. Ubuntu 설치 시 정한 유저 명의 폴더가 하나 있어야 함. 해당 폴더를 열기
    1. 열린 폴더에서 새로운 폴더를 만들기 (예. <code>db_project</code>)
    1. '터미널'에서 <code>ls</code> 명령어로 방금 만든 폴더가 존재하는지 확인
    1. 해당 폴더를 앞으로 '프로젝트 폴더'로 사용.

'프로젝트 폴더' 예시. db_project 를 확인

![Cap 2020-09-04 23-01-46-723](https://user-images.githubusercontent.com/32762296/92247791-ae367900-ef02-11ea-8705-36a4d4f0485b.jpg)
