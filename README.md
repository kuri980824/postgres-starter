# DB Homework

## Homework 0: Setup

### 목표

아래의 실습환경 중 **한 가지**를 선택하여 실습 환경을 구축하고 완료되었음을 보여주는 **스크린샷** 묶음과 자신이 설치한 실습 환경을 기입한 .txt 파일을 .zip으로 **압축하여 제출**하세요.

1. Docker Desktop for Windows (Pro, Enterprise, Education) / No WSL 2
1. Docker Desktop for Windows (Home+) / WSL 2
1. Docker Desktop for mac
1. Docker Engine / Docker Compose on Linux Distro
1. OS Native

<span style="color:red;">⚠ **주의!** 각 안내서에서 번호가 매겨진 항목은 빠짐없이 순서대로 실행하여야 합니다. ⚠</span>

⚠ 제공되는 실습 조건과 동일함을 보장하기 위해서는 Docker 설치를 강력히 권장합니다. 그러나 PC 사양 등의 문제로 OS 에 직접 필요한 프로그램을 설치할 수 있습니다. 그러나 실습환경 문제로 발생하는 문제는 조교가 곧바로 해결 불가할 수 있으며, **이후 모든 학습 안내서는 Docker 환경을 기본으로 작성됩니다.**

상기한 방법을 올바르게 구축했다면 다음의 서비스/프로그램이 **모두** 잘 설치되어 동작하고 있어야 합니다.

- Python 3.7 이상
- Git 2.26 이상

또는
- Docker 19 이상

터미널에서 각 프로그램의 버전을 출력하는 결과를 스크린샷으로 찍고, .zip으로 **압축** 해서 제출합니다.

자세한 내용은 [homework_0](https://gitlab.com/hyudblab/postgres-starter/-/tree/master/homework_0) 디렉토리로 이동하여 확인하세요.

## Homework 0-a: PostgreSQL Setup - 학부 / 데이터베이스시스템및응용

### 목표

Postgresql 을 설치하고 DB 에 접근해봅니다.

아래에 조건 중 자신의 환경에 따라 구축이 완료된 환경의 <u>스크린샷 **1개**</u>을 <code>homework_0</code>의 압축파일에 포함하여 제출하세요.

1. Docker 를 사용하는 환경
    - <code>docker-compose</code> 로 postgresql 실행해보고 CUI 방식으로 DB에 접근합니다.
1. Docker 를 사용하지 않는 환경
    - postgresql 을 직접 설치하고 CUI 방식으로 DB에 접근합니다.

자세한 내용은 [homework_0a](https://gitlab.com/hyudblab/postgres-starter/-/tree/master/homework_0a) 디렉토리로 이동하여 확인하세요.

## Homework 0-b: Jupyter Setup - 대학원 / 고급데이터베이스

### 목표

Jupyter notebook 으로 Python 을 개발할 수 있는 환경을 구축합니다.

아래의 조건으로 Jupyter notebook을 실행하고 <u>스크린샷 **1개**</u>를 <code>homework_0</code>의 압축파일에 포함하여 제출하세요.

1. Docker Jupyter stack 사용하기
    - Jupyter notebook 에서 pandas import 하기
    - 제시된 조건에 부합하는 DataFrame 만들고 출력하기

자세한 내용은 [homework_0b](https://gitlab.com/hyudblab/postgres-starter/-/tree/master/homework_0b) 디렉토리로 이동하여 확인하세요.


# QnA

- 블랙보드 질문란 활용
- Gitlab Issue 활용 (좌측 메뉴)

# 일러두기

- 막히는 부분이 있다면 질문을 활용하여 반드시 이번 과제를 완성하도록 합니다.
- 비대면으로 진행되는 학기 특성 상, 이번 과제 진행에 문제가 없어야 이후 실습이 원활해질 수 있습니다.