# Homework 0-b: Jupyter Notebook

## 목표

Jupyter 을 설치하고 접속해봅니다.

아래에 조건 중 자신의 환경에 따라 구축이 완료된 환경의 <u>스크린샷 **1개**</u>을 <code>homework_0</code>의 압축파일에 포함하여 제출하세요.

1. Docker 를 사용하는 환경
    - [Jupyter Docker Stack](https://jupyter-docker-stacks.readthedocs.io/en/latest/) 을 활용하여 Jupyter 를 실행해봅니다.
1. Docker 를 사용하지 않는 환경
    - [Jupyter 공식 문서](https://jupyter.org/install)를 따라 설치하고 실행해봅니다.

# Docker 사용 환경에서 실행

1. 각자 환경의 '프로젝트 폴더'에서 터미널을 열고 다음을 실행

```bash
docker run -p 8888:8888 jupyter/scipy-notebook
```

2. 터미널의 URL 정보(http://127.0.0.1:8888/?token= ...)를 토큰 값까지 전체 복사하여 웹 브라우저에서 입력하기

🎉 Jupyter 실행 완료

3. 하단의 과제 수행하기

## 실습 종료 - 컨테이너 제거

컨테이너의 수행이 종료되어도 컨테이너는 삭제되지 않고 stop 상태로 이전된다.

1. Jupyter 가 실행된 Docker 종료: 터미널에서 <code>Ctrl + C</code>

2. 다음 명령어로 jupyter 가 수행된 container 의 정보를 확인

```bash
docker ps -a
```

3. container ID 로 컨테이너 종료

```bash
docker rm [CONTAINER ID]
```

예시 

![Cap 2020-09-05 23-20-11-624](https://user-images.githubusercontent.com/32762296/92307015-5d8c5200-efce-11ea-9070-1f46786efa5e.jpg)


# Docker 사용하지 않는 환경

1. 터미널에서 '프로젝트 폴더'로 이동
2. 자신의 설치 방법에 따라 pandas 를 설치
   - conda: <code>conda install pandas</code> 
   - pip: <code>pip install pandas</code>
3. [공식 문서](https://jupyter.readthedocs.io/en/latest/running.html)를 따라 Jupyter notebook 을 실행

# 과제: 첫 pandas dataframe 

1. 아래 그림 처럼 <code>name</code>, <code>dept</code>, <code>status</code> 의 컬럼과 임의의 두개의 행을 가진 Dataframe 을 생성하고 표시해본다. 단, 첫 행의 <code>name</code>, <code>dept</code> 는 제출의 본인의 정보여야 한다.

![Cap 2020-09-05 22-38-10-464](https://user-images.githubusercontent.com/32762296/92306234-84e02080-efc8-11ea-8bc5-43735f64c190.jpg)

- DataFrame 오브젝트를 생성하는 방법은 여러가지가 있다.
  - [생성자](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html)
  - [클래스메소드 from_records](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.from_records.html#pandas.DataFrame.from_records)
  - [클래스메소드 from_dict](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.from_dict.html#pandas.DataFrame.from_dict)

2. 두번째 셀을 완성하고 실행한 스크린샷을 찍고 <code>homework_0</code> 의 결과와 같이 압축하여 제출