# Docker-compose: Postgresql + pgadmin

[공식 문서](https://docs.docker.com/compose/)

## Docker Compose ?

Docker Compose 는 미리 지정된 설정값에 따라 Docker Service 를 실행하고 관리할 수 있는 도구이다.

기본적으로 Docker 는 CLI (명령줄 인터페이스) 를 기반으로 실행하거나 Dockerfile 을 작성하여 이미지를 빌드하는 형태로도 사용할 수 있지만 Docker-compose 를 사용하면 더 편리하게 기존의 이미지나 빌드 툴을 사용할 수 있다.

요약하자면,

- Docker CLI: 터미널에서 <code>docker</code> 명령어로 진행하는 docker 실행 방법. 가장 기초적인 실행법이다.
- Dockerfile: docker 실행 명령을 순서대로 나열해놓은 스크립트에 가까운 사용 방법.
- Docker Compose: <code>.yml</code> 코드에 명시된 설정대로 docker 서비스를 실행하는 방법.


## docker-compose.yml

Docker Compose 는 터미널에서 <code>docker-compose</code> 로 실행한다. 별도의 옵션이 없으면 명령이 실행되는 현재 위치의 <code>docker-compose.yml</code> 파일의 내용대로 docker 를 실행한다. <code>up</code> 옵션을 통해 실행하고 <code>down</code> 옵션을 통해 종료한다.

별다른 언급이 없는 경우, 자습서의 compose 실행은 다음 명령어를 의미한다.

```bash
docker-compose up -d
```

마찬가지로 별다른 언급이 없는 경우, 자습서의 compose 종료는 다음 명령어를 의미한다.

```bash
docker-compose down -v
```

### up -d 옵션

[참고](https://docs.docker.com/compose/reference/up/)

<code>-d</code> 는 백그라운드 실행 옵션으로 없는 경우 포어그라운드로 docker 가 실행된다. 따라서 compose를 실행한 터미널을 종료하면 모든 container 가 종료된다.

### down -v 옵션

[참고](https://docs.docker.com/compose/reference/down/)

<code>-v</code> 은 <code>docker-compose.yml</code> 에 이름이 지정된 volumes 들을 삭제하면서 compose를 종료한다. 따라서 컨테이너가 명시된 volume 을 사용하는 경우 데이터가 모두 날아가므로 주의. 컨테이너가 볼륨에 남긴 데이터를 활용할 생각이라면 -v 옵션 없이 종료해야한다.

### services

compose 가 실행되면 현재 위치의 docker-compose.yml 의 정보를 토대로 docker 명령이 실행된다.

services 항목에는 실행될 container 의 속성을 명시한다.

yml 파일은 일종의 문법이 있는 마크업 언어이며, Python 과 같이 인덴트(들여쓰기)로 섹션이 구분된다.

예시

```yml
services:
  postgres:
    image: postgres:12    
    environment:
      POSTGRES_PASSWORD: ${PG_PASSWORD:-1234}
      POSTGRES_INITDB_ARGS: --encoding=UTF-8
```

<code>services</code> 섹션

- compose 예약어
- 실행될 container 들이 나열됨.

<code>postgres</code> 섹션

- 실행될 서비스명. 즉, 사용자가 직접 짓는 변수명이다. **겹치지만 않으면 이론적으로 아무렇게나 써도 된다.**
- 같은 컨테이너는 네트워크에서 이 서비스를 <code>postgres</code> 로 접근할 수 있다.
  - 가령, 해당 서비스가 웹 서버를 실행 중일 경우 <code>http://postgres</code> 는 이 컨테이너에 http 요청을 보낸다는 의미이다.

<code>image</code> 값

- CLI 에서 <code>docker run</code>의 <code>IMAGE</code> 변수
- 이미 만들어져있는 Docker 이미지 이름을 적는다.
- 저장소가 생략되어있을 경우 dockerhub 을 우선으로 한다. 즉, <code>postgres:12</code> 라고 적으면 dockerhub 저장소의 postgres 이미지 중 12 태그가 붙은 이미지를 받아온다.
- 이미 만들어진 이미지가 없을 경우 이 값 대신 <code>build:</code> 로 dockerfile 을 지정할 수 있다.

<code>environment</code>

- CLI 의 <code>-e</code> 옵션
- 컨테이너가 실행될 때 사용될 환경변수 모음이다.
- 해당 환경 변수가 어떻게 사용될지는 전적으로 컨테이너 실행 이미지 (또는 빌드된 Dockerfile) 에 의존하므로 해당 값이 어떻게 쓰이는지는 이미지 제작자의 설명을 따른다.
- 위의 예시의 경우 [postgres 공식 도커 이미지](https://hub.docker.com/_/postgres) 의 'Environment Variables' 항목에서 제시한 대로 적용하였다.

## .env

[원본 문서](https://docs.docker.com/compose/environment-variables/#the-env-file)

<code>docker-compose.yml</codee>에서 특정 설정값을 파일 내에 명시하지 않고 실행되는 위치에 따라 변수를 변경하기 위한 환경변수 파일이다.

예시:
```
# docker-compose.yml
...
environment:
    POSTGRES_PASSWORD: ${PG_PASSWORD:-1234}
...
```
이 경우 <code>.env</code> 파일의 <code>PG_PASSWORD</code> 값이 설정되어있으면 해당 값으로 <code>POSTGRES_PASSWORD</code> 값으로 넘겨진다. 그렇지 않을 경우 <code>1234</code> 가 기본값으로 설정된다.