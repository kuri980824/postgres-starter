# Homework 0-a: PostgreSQL

## 목표

Postgresql 을 설치하고 DB 에 접근해봅니다.

아래에 조건 중 자신의 환경에 따라 구축이 완료된 환경의 <u>스크린샷 **1개**</u>을 <code>homework_0</code>의 압축파일에 포함하여 제출하세요.

1. Docker 를 사용하는 환경
    - <code>docker-compose</code> 로 postgresql 실행해보고 CUI 방식으로 DB에 접근합니다.
1. Docker 를 사용하지 않는 환경
    - postgresql 을 직접 설치하고 CUI 방식으로 DB에 접근합니다.

## Docker 사용 환경

[docker 디렉토리](https://gitlab.com/hyudblab/postgres-starter/-/tree/master/homework_0a/docker) 안내 참조


## Docker 사용하지 않는 환경

[블랙보드](learn.hanyang.ac.kr) 의 실습 1주차에서 자료에서 자신의 환경에 맞는 방법으로 PostgreSQL 을 설치합니다.

설치가 완료되면 각자의 터미널에서 psql 명령을 실행할 수 있습니다.

또는 psql 이 실제 설치된 경로 (예: <code>C:\Program Files\PostgreSQL\bin</code>) 등에서 psql 실행 파일 (윈도우라면 <code>psql.exe</code>) 을 터미널에서 실행하세요.

## 과제: psql 명령으로 DB 정보 접속하고 스크린샷 찍기

각 환경에서 psql 에 진입할 수 있는 환경에서

1. psql 진입

```bash
psql -U postgres
```
2. 현재 테이블 보기 

```bash
\l
```

3. ✔ 현재 화면을 캡처하여 스크린샷을 과제에 포함

실행 예시

![Cap 2020-09-05 00-01-07-584](https://user-images.githubusercontent.com/32762296/92253835-f9548a00-ef0a-11ea-84dd-ce063ed3db82.jpg)

4. psql 종료

```bash
\q
```