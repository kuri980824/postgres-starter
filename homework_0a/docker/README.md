# Docker 로 Postgresql 과 pgadmin 컨테이너 실행

## Docker 컨테이너 실행

자신의 '프로젝트 폴더'에서 '터미널'을 열고 다음을 차례대로 실행

1. <code>git</code> 으로 외부 저장소에서 프로젝트 복사/다운로드하기.
```bash
git clone https://gitlab.com/hyudblab/postgres-starter.git
```
2. 다운로드된 폴더로 이동
```bash
cd postgres-starter/pg_pgamdin
```
3. 미리 지정된 내용으로 docker 컨테이너 실행
```bash
docker-compose up -d
```

### 실행 중인 Docker 컨테이너 정보 보기

```bash
docker ps
```

실행 예시

![Cap 2020-09-04 23-53-34-377](https://user-images.githubusercontent.com/32762296/92252926-dfff0e00-ef09-11ea-9efd-38b49c101b94.jpg)

## 실행 중인 Docker 컨테이너로 진입하기

1. <code>docker ps</code> 실행 결과에서 보이는 <code>IMAGE</code> 값이 <code>postgres:12</code> 인 컨테이너의 <code>CONTAINER ID</code> 를 확인한다
    - 위의 예시 기준 <code>86a35c1cec12</code> 이다.
1. 다음 명령어에 <code>postgres:12</code> 컨테이너의 id 를 <code>[CONTAINER ID]</code>에 대신 넣어 실행
   - 컨테이너 ID 가 중복되지 않는 선에서 첫 두글자 이상만 입력해도 된다.
```bash
docker exec -it [CONTAINER ID] /bin/bash
```   
예시

```bash
docker exec -it 86a /bin/bash
```

## 과제 수행하기


1. [homework_0a](https://gitlab.com/hyudblab/postgres-starter/-/tree/master/homework_0a) 안내대로 스크린샷 찍기

## Docker container 에서 빠져나오고 container 모두 종료하기

과제 수행 안내대로 실행했을 경우 container 내부의 shell 일 것이다. 즉, 현재 터미널이 다음과 유사한 형태여야 함.

```bash
root@[컨테이너ID]:/#
```

컨테이너 ID는 docker가 HASH 값으로 의미없는 영문숫자의 나열이다.

1. 컨테이너 쉘 종료

```bash
exit
```

컨테이너 쉘을 종료하면 터미널에 <code>root@[컨테이너ID]</code> 대신 자신의 <code>사용자 이름@컴퓨터 명</code> 으로 변경될 확률이 높다(Ubuntu bash 기본값. 쉘 설정마다 다름.)

2. 실행된 컨테이너들 모두 종료

```bash
docker-compose down -v
```

3. 컨테이너가 종료됨을 확인

```bash
docker ps -a
```

실행 예시
![Cap 2020-09-05 00-06-00-932](https://user-images.githubusercontent.com/32762296/92254280-9b747200-ef0b-11ea-9354-5aa760ea16f7.jpg)

🎉🎉 실행 완료! 🎉🎉

🚠 방금 실행한 docker-compose 에 대해서는 [pg_pgadmin](https://gitlab.com/hyudblab/postgres-starter/-/tree/master/pg_pgamdin) 디렉토리의 README 를 참조
