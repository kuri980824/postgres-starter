# 과제 1

## 목표

<code>query</code> 를 입력받아 해당 키워드가 포함된 <code>Menu</code>를 판매하는 <code>Seller</code> 와 <code>Store</code> 정보를 찾는 앱을 완성하세요.
- <code>app.py</code> 의 <code>TODO</code> 를 완성하여 아래 예시와 같이 동작하는 코드를 완성하세요.


## 제출물

- <code>data</code> 폴더를 제외하고 <code>app.py</code> 가 포함된 <code>project</code> 를 압축해서 제출하세요.
    - 압축은 .zip 또는 .tar 로만 압축하세요.
- 채점 기준 환경은 Linux 환경의 <code>Python 3.8</code> 입니다. (docker-compose.yml 환경과 동일)


## 상세 설명

### 코드
main 함수는 Command line argument 인 <code>query</code> 를 인자로 받고 두 개의 함수를 실행한 뒤 <code>results</code> 를 출력합니다.
과제는 <code>results</code> 에 올바를 값이 출력되도록 두 함수를 완성하는 것입니다.

### 실행

```bash
python app.py query
# 예시
# python app.py 우유
```


#### read_data()

```python
def read_data():
    sellerdata = read_csv("data/seller.csv")
    storedata = read_csv("data/store.csv")
    menudata = read_csv("data/menu.csv")

    # TODO: Create and return database to query
    maybe_useful_database = None
    return maybe_useful_database
```

입력: 없음

출력: 
<code>find_store_having_menu_with()</code> 가 사용할 수 있는 형태로 읽어들인 csv 데이터를 적절히 가공하여 리턴

힌트?:
- read_data 에서 꼭 무슨 처리를 반드시 해야할 필요는 없음. 읽어진 데이터를 튜플로 묶어 통째로 리턴해도 상관없음.

#### find_store_having_menu_with()

```python
def find_store_having_menu_with(query, dataset, *args, **kwargs):
    # TODO: Find information like below by query
    # Seller info: name, phone, email=(local@domain)
    # Store info: name, address
    # Menu info: menu
    results = []
    return results
```

입력 
- query: 메뉴(menu.csv의 menu column)에서 검색할 단어
- dataset: <code>read_data()</code> 로부터 읽은 csv 데이터들

출력
- 동작 예시 참고


## 동작 예시

![Cap 2020-10-09 10-17-24-503](https://user-images.githubusercontent.com/32762296/95531234-00dfe500-0a1b-11eb-9bce-2141cd4d9f06.jpg)
