import argparse
import time
from pprint import pprint
from csv_helper import read_csv


def read_data():
    sellerdata = read_csv("data/seller.csv")
    storedata = read_csv("data/store.csv")
    menudata = read_csv("data/menu.csv")

    # TODO: Create and return database to query
    maybe_useful_database = None
    return maybe_useful_database


def find_store_having_menu_with(query, dataset, *args, **kwargs):
    # TODO: Find information like below by query
    # Seller info: name, phone, email=(local@domain)
    # Store info: name, address
    # Menu info: menu
    results = []
    return results


def main(query):
    database = read_data()
    result = find_store_having_menu_with(query, database)
    pprint(result)


if __name__ == "__main__":
    start = time.time()
    parser = argparse.ArgumentParser()
    parser.add_argument("query", help="Find a menu containing this word")
    args = parser.parse_args()
    main(args.query)
    print("Running Time: ", end="")
    print(time.time() - start)
